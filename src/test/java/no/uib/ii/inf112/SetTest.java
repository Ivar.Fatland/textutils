package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.impl.MySetImpl;
import no.uib.ii.inf112.Set;

public class SetTest {

	@Test
	void testAdd() {
		String[] strings = { "mango", "banan", "ananas" };
		Set<String> set1 = new MySetImpl<>();
		for (String s : strings)
			addContainsExistingProperty(set1, "mango", s);
		for (String s : strings)
			addContainsProperty(set1, s);

	}

	<T> void addContainsProperty(Set<T> set, T elem) {
		set.add(elem);
		assertTrue(set.contains(elem));
	}

	<T> void addContainsExistingProperty(Set<T> set, T existingElem, T elem) {
		boolean existing = set.contains(existingElem);
		set.add(elem);
		assertTrue(set.contains(elem));
		if (!existingElem.equals(elem))
			assertEquals(existing, set.contains(existingElem),
					String.format("contains(%s) == %b, add %s", existingElem.toString(), existing, elem.toString()));
	}

	@Test
	void testSize() {
		Set<String> set = new MySetImpl<>();
		set.add("mango");
		assertEquals(1, set.size());
		set.add("banan");
		assertEquals(2, set.size());
	}

	@Test
	void testSizeAddExisting() {
		Set<String> set = new MySetImpl<>();
		set.add("mango");
		assertEquals(1, set.size());
		set.add("mango");
		assertEquals(1, set.size());
	}

}
